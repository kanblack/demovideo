package com.dev.kb.demovideocrop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

/**
 * Created by kanblack on 11/20/15.
 */
public class MenuActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onGotoRecoder(View v) {
        startActivity(new Intent(this, VideoRecoderActivity.class));
    }

    public void onGoToPicker(View v) {
        startActivity(new Intent(this, VideoEditorActivity.class));
    }
}
