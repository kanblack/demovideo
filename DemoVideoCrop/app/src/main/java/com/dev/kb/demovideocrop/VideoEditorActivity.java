package com.dev.kb.demovideocrop;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kanblack on 11/20/15.
 */
public class VideoEditorActivity extends FragmentActivity implements MediaPlayer.OnVideoSizeChangedListener, MediaPlayer.OnPreparedListener {
    boolean isVideoVertical;
    boolean surfaceReady;
    private static final int IMPORT_FROM_GALLERY_REQUEST = 199;
    private MediaPlayer mediaPlayer;
    private SurfaceView playerSurfaceView;
    private SurfaceView mSurfaceView;
    private SurfaceHolder surfaceHolder;

    int videoWidth, videoHeight;
    int statusBarHeight;
    private Uri videoSrc;

    private View viewBg;
    private Point devicesize;
    private int currentX, currentY;
    private FFmpeg ffmpeg;
    private int sizeTarget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoview);
        mSurfaceView = (SurfaceView) findViewById(R.id.playerSurface);
        viewBg = findViewById(R.id.viewBg);

        playerSurfaceView = (SurfaceView) findViewById(R.id.playerSurface);
        statusBarHeight = getStatusBarHeight();
        surfaceHolder = playerSurfaceView.getHolder();
        surfaceHolder.addCallback(surfaceCallBack);

        try {
            ffmpeg = FFmpeg.getInstance(this);
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {
                }

                @Override
                public void onFailure() {
                }

                @Override
                public void onSuccess() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }

        initAddClip();
        getDeviceSize();
        setupTouchControl();

        findViewById(R.id.btnPlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                    } else {
                        mediaPlayer.start();
                    }
                }
            }
        });

        findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSaveVideoCrop();
            }
        });
    }

    private SurfaceHolder.Callback surfaceCallBack = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            surfaceReady = true;
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            surfaceReady = true;
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            surfaceReady = false;
        }
    };

    float startX, startY;
    float currentTop, currentLeft;

    private void setupTouchControl() {
        mSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                float rawX = event.getRawX();
                float rawY = event.getRawY();
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mSurfaceView.getLayoutParams();

                if (action == MotionEvent.ACTION_DOWN) {
                    currentTop = lp.topMargin;
                    currentLeft = lp.leftMargin;
                    startX = rawX;
                    startY = rawY;
                } else if (action == MotionEvent.ACTION_MOVE) {
                    if (isVideoVertical) {
                        float moveY = rawY - startY + currentTop;
                        lp.topMargin = (int) Math.max(devicesize.x - mSurfaceView.getHeight(), Math.min(0, moveY));
                        mSurfaceView.requestLayout();
                        currentX = 0;
                        currentY = (int) Math.abs(currentTop);
                    } else {
                        float moveX = rawX - startX + currentLeft;
                        lp.leftMargin = (int) Math.max(devicesize.x - mSurfaceView.getWidth(), Math.min(0, moveX));
                        mSurfaceView.requestLayout();
                        currentY = 0;
                        currentX = (int) Math.abs(currentLeft);
                    }
                } else if (action == MotionEvent.ACTION_CANCEL) {

                }
                return true;
            }
        });
    }

    public void getDeviceSize() {
        devicesize = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(devicesize);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) viewBg.getLayoutParams();
        lp.height = devicesize.x;
        lp.width = devicesize.x;
        viewBg.requestLayout();
    }

    private void initAddClip() {
        Button addClipButton = (Button) findViewById(R.id.btnChoose);
        addClipButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
                intent.setType("video/*");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent, IMPORT_FROM_GALLERY_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == IMPORT_FROM_GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri selectedVideo = intent.getData();
            if (selectedVideo == null) {
                return;
            }

            playVideo(selectedVideo);
        }
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        videoWidth = width;
        videoHeight = height;
        Log.e("video size", String.valueOf(videoWidth) + "x" + String.valueOf(videoHeight));

        if (mediaPlayer.isPlaying()) {
            surfaceHolder.setFixedSize(videoWidth, videoHeight);
        }
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mSurfaceView.getLayoutParams();
        if (isVideoVertical = videoIsVertical(videoWidth, videoHeight)) {
            sizeTarget = videoWidth;
            lp.width = devicesize.x;
            lp.height = videoHeight * devicesize.x / videoWidth;
        } else {
            sizeTarget = videoHeight;
            lp.height = devicesize.x;
            lp.width = videoWidth * devicesize.x / videoHeight;
        }

        currentLeft = lp.leftMargin = 0;
        currentTop = lp.topMargin = 0;
        Log.e("surface size", lp.width + "x" + lp.height);
        mSurfaceView.setLayoutParams(lp);
    }

    private boolean videoIsVertical(int videoWidth, int videoHeight) {
        if (videoWidth < videoHeight) {
            return true;
        }
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        surfaceHolder.setFixedSize(videoWidth, videoHeight);
        mediaPlayer.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    Handler mHandler;

    void playVideo(final Uri videoSrc) {
        if (mHandler == null) {
            mHandler = new Handler(getMainLooper());
        }

        this.videoSrc = videoSrc;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (surfaceReady) {
                    try {
                        mediaPlayer = new MediaPlayer();
                        mediaPlayer.setDisplay(surfaceHolder);
                        mediaPlayer.setDataSource(VideoEditorActivity.this, videoSrc);
                        mediaPlayer.prepare();
                        mediaPlayer.setOnPreparedListener(VideoEditorActivity.this);
                        mediaPlayer.setOnVideoSizeChangedListener(VideoEditorActivity.this);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    mHandler.postDelayed(this, 100);
                }
            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void doSaveVideoCrop() {
        setCropVideo(getRealPathFromURI(videoSrc), getOutPutPath().getAbsolutePath(), sizeTarget, sizeTarget, currentX, currentY);
    }

    private File getOutPutPath() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES), "DemoVideoCrop");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "VID_" + timeStamp + ".mp4");

        return mediaFile;
    }

    public void setCropVideo(String videoPath, final String outputPath, int widthTarget, int heightTarget, int startX, int startY) {
        final String cmd = String
                .format("-i %s -filter:v crop=%d:%d:%s:%s -vcodec libx264 -profile:v baseline -preset ultrafast -crf 28 -f mp4 -ar 44100 -strict -2 %s",
                        videoPath, widthTarget, heightTarget, startX, startY, outputPath);
        try {
            final ProgressDialog progressDialog = ProgressDialog.show(this, "Video crop", "processing", false, false);
            progressDialog.setCanceledOnTouchOutside(false);


            ffmpeg.execute(cmd, new ExecuteBinaryResponseHandler() {
                @Override
                public void onStart() {
                    Log.e("onStart", "Started command : ffmpeg " + cmd);
                }

                @Override
                public void onFailure(String s) {
                    Log.e("error", s);
                    progressDialog.dismiss();
                }

                @Override
                public void onSuccess(String s) {
                    Log.e("Success", s);
                    progressDialog.dismiss();
                }

                @SuppressLint("SimpleDateFormat")
                @Override
                public void onProgress(String s) {
                }

                @SuppressLint("SimpleDateFormat")
                @Override
                public void onFinish() {
                    Log.d("onFinish", "Finished command : ffmpeg " + cmd);
                    progressDialog.dismiss();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
