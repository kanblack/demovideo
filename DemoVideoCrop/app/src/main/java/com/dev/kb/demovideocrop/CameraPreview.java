package com.dev.kb.demovideocrop;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;

import java.util.List;

class CameraPreview extends FrameLayout implements SurfaceHolder.Callback {

    SurfaceView mSurfaceView;
    SurfaceHolder mHolder;
    private Camera mCamera;
    private List<Camera.Size> mSupportedPreviewSizes;
    private Camera.Size mPreviewSize;
    private Camera.Size mVideoSize;
    private String TAG = "AAAAA";

    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;
        mSurfaceView = new SurfaceView(context);
        addView(mSurfaceView);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            if (mPreviewSize == null) {
                getBestPreviewSize(parameters);
            }

            parameters.setPreviewSize(mPreviewSize.width,mPreviewSize.height);
            mCamera.setParameters(parameters);
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
        setLayoutParams(new FrameLayout.LayoutParams(VideoRecoderActivity.devicesize.x, VideoRecoderActivity.devicesize.x*mPreviewSize.width/mPreviewSize.height));
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        if (mCamera != null) {
            // Call stopPreview() to stop updating the preview surface.
            mCamera.stopPreview();
        }
    }

    private void getBestPreviewSize(Camera.Parameters parameters) {
        Camera.Size bestPreviewSize = null;
        Camera.Size bestVideoSize = null;
        List<Camera.Size> previewSizeList = parameters.getSupportedPreviewSizes();
        List<Camera.Size> videoSizeList = parameters.getSupportedPreviewSizes();


        for (Camera.Size size : videoSizeList) {
            if (size.width <= 640) {
                if (bestVideoSize == null) {
                    bestVideoSize = size;
                } else if (size.width > bestVideoSize.width) {
                    bestVideoSize = size;
                }
            }

        }

        Log.e("bestVideoSize w/h", bestVideoSize.width + "/" + bestVideoSize.height);

        for (Camera.Size size : previewSizeList) {
            if ((double) size.width / (double) size.height == (double) bestVideoSize.width / (double) bestVideoSize.height) {
                Log.e((double) bestVideoSize.width / (double) bestVideoSize.height + "", (double) size.width / (double) size.height + "");

                if (bestPreviewSize == null) {
                    bestPreviewSize = size;
                } else if (size.width > bestPreviewSize.width) {
                    bestPreviewSize = size;
                }
            }
        }
        Log.e("bestPreviewSize w/h", bestPreviewSize.width + "/" + bestPreviewSize.height);

        mPreviewSize = bestPreviewSize;
        mVideoSize = bestVideoSize;
    }


    public SurfaceHolder getHolder() {
        return mSurfaceView.getHolder();
    }

    private void stopPreviewAndFreeCamera() {

        if (mCamera != null) {
            // Call stopPreview() to stop updating the preview surface.
            mCamera.stopPreview();

            // Important: Call release() to release the camera for use by other
            // applications. Applications should release the camera immediately
            // during onPause() and re-open() it during onResume()).
            mCamera.release();

            mCamera = null;
        }
    }

    public static void getDeviceSize(Activity activity, Point devicesize) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getSize(devicesize);
    }
}